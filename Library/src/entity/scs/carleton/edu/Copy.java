package entity.scs.carleton.edu;

public class Copy {
private String isbn;
private int copyNumber;
public Copy(String isbn,int copynumber){
	this.setCopyNumber(copynumber);
	this.setIsbn(isbn);
}
public String getIsbn() {
	return isbn;
}
public void setIsbn(String isbn) {
	this.isbn = isbn;
}
public int getCopyNumber() {
	return copyNumber;
}
public void setCopyNumber(int copyNumber) {
	this.copyNumber = copyNumber;
}

}
