package entity.scs.carleton.edu;

public class Title {
private String isbn;
private int copyCount;
public Title(String isbn, int copyCount){
	this.setIsbn(isbn);
	this.setCopyCount(copyCount);
}
public String getIsbn() {
	return isbn;
}
public void setIsbn(String isbn) {
	this.isbn = isbn;
}
public int getCopyCount() {
	return copyCount;
}
public void setCopyCount(int copyCount) {
	this.copyCount = copyCount;
}
}
