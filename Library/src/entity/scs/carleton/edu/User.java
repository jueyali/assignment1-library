package entity.scs.carleton.edu;

public class User {
private String userId;
private String userName;
private int booksKept;
private int fine;
public User(String userId, String userName, int booksKept, int fine){
	this.setUserId(userId);
	this.setUserName(userName);
	this.setBooksKept(booksKept);
	this.setFine(fine);
}
public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public int getBooksKept() {
	return booksKept;
}
public void setBooksKept(int booksKept) {
	this.booksKept = booksKept;
}
public int getFine() {
	return fine;
}
public void setFine(int fine) {
	this.fine = fine;
}
}
