package test.scs.carleton.edu;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.scs.carleton.edu.*;

public class TestLogIn {
	UserDAO userdao;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestLogIn");
    }
    
    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestLogIn");
		userdao=new UserDAO();
		userdao.Initiate();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestLogIn");
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestLogIn");
    }
    
    @Test
	public void ValidLogin() {
		System.out.println("@Test(): Valid");
		assertEquals(true,userdao.LogIn("123456"));
	}
    
    @Test
    public void InvalidLogin01(){
    	System.out.println("Test():Invalid01");
    	assertEquals(false,userdao.LogIn("asdhueod"));
    }
}
