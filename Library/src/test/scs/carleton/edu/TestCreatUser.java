package test.scs.carleton.edu;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.scs.carleton.edu.*;
import entity.scs.carleton.edu.*;

public class TestCreatUser {
	UserDAO userdao;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestCreatUser");
    }
    
    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestCreatUser");
		userdao=new UserDAO();
		userdao.Initiate();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestCreatUser");
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestCreatUser");
    }
    
    @Test
	public void ValidCreatUser() {
		System.out.println("@Test(): Valid");
		User newuser= new User("654321","Ryan",0,0);
		assertEquals(true,userdao.CreatUser(newuser));
		assertEquals(2,userdao.getTotalusers());
		assertEquals(newuser,userdao.getUsers()[1]);
	}
    
    @Test
    public void InvalidCreatUser01(){
    	System.out.println("Test():Invalid01:Existed Userid");
    	User newuser= new User("123456","Ryan",0,0);
		assertEquals(false,userdao.CreatUser(newuser));
		assertEquals(1, userdao.getTotalusers());
    }
}

