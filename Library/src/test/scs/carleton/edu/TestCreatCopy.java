package test.scs.carleton.edu;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.scs.carleton.edu.*;
import entity.scs.carleton.edu.*;

public class TestCreatCopy {
	CopyDAO copydao;
	TitleDAO titledao;
	Title[] alltitles;
	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestCreatCopy");
    }
    
    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestCreatCopy");
		copydao=new CopyDAO();
		titledao=new TitleDAO();
		copydao.Initiate();
		titledao.Initiate();
		alltitles= titledao.getTitles();
		System.out.println(alltitles.length);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestCreatCopy");
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestCreatCopy");
    }
    
    @Test
	public void ValidCreatCopies() {
		System.out.println("@Test(): Valid");
		Copy newcopy= new Copy("Y9783319068619",4);
		assertEquals(true,copydao.AddCopy(newcopy,alltitles));
	}
    
    @Test
    public void InvalidCreatCopies01(){
    	System.out.println("Test():Invalid01:Existed isbn & copynumber pair");
    	Copy newcopy= new Copy("Y9783319068619",1);
		assertEquals(false,copydao.AddCopy(newcopy,alltitles));
    }
    @Test
    public void InvalidCreatCopies02(){
    	System.out.println("Test():Invalid02:Non-existed isbn");
    	Copy newcopy= new Copy("Y9783319068622",1);
		assertEquals(false,copydao.AddCopy(newcopy,alltitles));
    }
}

