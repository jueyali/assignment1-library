package test.scs.carleton.edu;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.scs.carleton.edu.*;
import entity.scs.carleton.edu.*;

public class TestCreatTitles {
	TitleDAO titledao;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestCreatTitles");
    }
    
    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestCreatTitles");
		titledao=new TitleDAO();
		titledao.Initiate();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestCreatTitles");
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestCreatTitles");
    }
    
    @Test
	public void ValidCreatTitles() {
		System.out.println("@Test(): Valid");
		Title newtitle= new Title("Y9783319068621",4);
		assertEquals(true,titledao.AddTitle(newtitle));
		assertEquals(2,titledao.getTotaltitles());
	}
    
    @Test
    public void InvalidCreatTitles01(){
    	System.out.println("Test():Invalid01:Existed isbn");
    	Title newtitle= new Title("Y9783319068619",5);
		assertEquals(false,titledao.AddTitle(newtitle));
		assertEquals(1, titledao.getTotaltitles());
    }
}
