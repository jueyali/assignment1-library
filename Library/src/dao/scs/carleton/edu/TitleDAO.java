package dao.scs.carleton.edu;

import entity.scs.carleton.edu.Title;

public class TitleDAO {
private Title[] titles;
private int totaltitles;
public Title[] getTitles() {
	return titles;
}
public void setTitles(Title[] titles) {
	this.titles = titles;
}
public int getTotaltitles() {
	return totaltitles;
}
public void setTotaltitles(int totaltitles) {
	this.totaltitles = totaltitles;
}
public Title[] Initiate(){
	this.titles=new Title[100000];
	Title starttitle= new Title("Y9783319068619",3);
	this.AddTitle(starttitle);
	return titles;
}
public boolean AddTitle(Title newtitle){
	Title[] alltitles= this.getTitles();
	for(int i=0;i<this.getTotaltitles();i++){
		if(alltitles[i].getIsbn()==newtitle.getIsbn()){
			return false;
		}
	}
	alltitles[totaltitles]= newtitle;
	this.setTotaltitles(this.getTotaltitles()+1);
	return true;
}
public Title SelectTitleByisbn(String isbn){
	Title result= new Title(null,0);
	for(int i=0;i<this.getTotaltitles();i++)
	{
		if(this.titles[i].getIsbn()==isbn){
			result=this.titles[i];
		}
	}
	return result;
}
}
