package dao.scs.carleton.edu;

import entity.scs.carleton.edu.User;

public class UserDAO {
private User[] users;
private int totalusers;
public User[] getUsers() {
	return users;
}
public void setUsers(User[] users) {
	this.users = users;
}
public int getTotalusers() {
	return totalusers;
}
public void setTotalusers(int totalusers) {
	this.totalusers = totalusers;
}
public User[] Initiate(){
	this.users= new User[1000];
	User startuser= new User("123456","Zoey",0,0);
	this.CreatUser(startuser);
	return users;
};
public boolean CreatUser(User newuser){
	User[] allusers= this.getUsers();
	int totaluser=this.getTotalusers();
	for(int i=0;i<totaluser;i++){
		if(allusers[i].getUserId()==newuser.getUserId())
			return false;
	}
	allusers[totaluser]= newuser;
	this.setTotalusers(this.getTotalusers()+1);
	return true;
}
public Boolean LogIn(String Userid){
	Boolean result=false;
	for(int i=0;i<this.totalusers;i++){
		if(this.users[i].getUserId()==Userid){
			result=true;
		}
	}
	return result;
}
public User SelectUserById(String Userid){
	User result=new User(null,null,0,0);
	for(int i=0;i<this.totalusers;i++){
		if(this.users[i].getUserId()==Userid){
			result=this.users[i];
		}
	}
	return result;
}
}
