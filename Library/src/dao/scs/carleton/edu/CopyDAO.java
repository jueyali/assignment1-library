package dao.scs.carleton.edu;

import entity.scs.carleton.edu.Copy;
import entity.scs.carleton.edu.Title;

public class CopyDAO {
private Copy[] copies;
private int totalcopies;
public Copy[] getCopies() {
	return copies;
}
public void setCopies(Copy[] copies) {
	this.copies = copies;
}
public int getTotalcopies() {
	return totalcopies;
}
public void setTotalcopies(int totalcopies) {
	this.totalcopies = totalcopies;
}
public Copy[] Initiate(){
	this.copies=new Copy[1000000];
	Copy startcopy=new Copy("Y9783319068619",1);
	this.copies[0]=startcopy;
	this.totalcopies=1;
	return copies;
}
public boolean AddCopy(Copy newcopy, Title[] alltitles){
	Copy[] allcopies= this.getCopies();
	for(int i=0;i<this.getTotalcopies();i++){
		if (allcopies[i].getIsbn()==newcopy.getIsbn() && allcopies[i].getCopyNumber()==newcopy.getCopyNumber())
		{
			return false;
		}
	}
	int j=0;
	while(alltitles[j]!=null){
		 if(alltitles[j].getIsbn()==newcopy.getIsbn()){
			allcopies[this.getTotalcopies()]=newcopy;
			this.setTotalcopies(this.getTotalcopies()+1);
			return true;
		}
		 j++;
	}
	return false;
}
public Copy SelectCopyByPair(String isbn, int copynumber){
	Copy result=new Copy(null,0);
	for(int i=0;i<this.getTotalcopies();i++){
		if(this.getCopies()[i].getIsbn()==isbn && this.getCopies()[i].getCopyNumber()==copynumber){
			result=this.getCopies()[i];
		}
	}
	return result;
}
}
